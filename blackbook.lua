BlackBook = {}
BlackBook.KillersList = {}
BlackBook.RecentTargets ={}
BlackBook.PlayerSuicides = 0

PlayerName = string.sub(WStringToString(GameData.Player.name),1,-3)
LangDef = 0

local InBG = false
local c_HOSTILE_TARGET = "selfhostiletarget"
 local oldHostileName = nil
 local old = nil

 function BlackBook.ResetLocals()
 oldHostileName = nil
 old = nil
 BlackBook.RecentTargets = {}
 end
function BlackBook.Initialize()
	 
	
	LibSlash.RegisterSlashCmd("bb", function(input) BlackBook.Slash(input) end)
	TextLogAddEntry("Chat", 0, L"Little Black Book Loaded");
	
 	RegisterEventHandler(SystemData.Events.PLAYER_DEATH, "BlackBook.PlayerDeath");
	RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_UPDATED, "BlackBook.PlayerRenownUpdated")
	
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED, "BlackBook.TargetUpdate")
	RegisterEventHandler(SystemData.Events.PLAYER_DEATH_CLEARED, "BlackBook.DeathCleared")
	
	RegisterEventHandler(SystemData.Events.SCENARIO_BEGIN, "BlackBook.UpdateScenario")
	RegisterEventHandler(SystemData.Events.SCENARIO_END, "BlackBook.UpdateScenario")
	
	
end
local systemTime = 0
local updateTime = 0
function BlackBook.Update(elapsed)

	systemTime = systemTime + elapsed
	
	if (systemTime - updateTime) > 0.5 then

		updateTime = systemTime
	end

end

function BlackBook.Slash(action)

	local input1 = string.sub(action,0,string.find(action," "))
	EA_ChatWindow.Print(towstring(input1));
	if string.find(action," ") ~= nil then
		input1 = string.sub(action,0,string.find(action," ")-1)
		input2 = string.sub(action,string.find(action," ")+1,-1)
	end

	if input1 == "show" then
		BlackBookWindow.Show()
	elseif input1 == "output" then
		BlackBook.PrintKillList()
	elseif input1 == "reset" then
		BlackBook.ResetList()
	end
end

function BlackBook.Shutdown()

 	UnregisterEventHandler(SystemData.Events.PLAYER_DEATH, "BlackBook.PlayerDeath");
	UnregisterEventHandler(SystemData.Events.PLAYER_RENOWN_UPDATED, "BlackBook.PlayerRenownUpdated");
	UnregisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED, "BlackBook.TargetUpdate")
	UnregisterEventHandler(SystemData.Events.SCENARIO_BEGIN, "BlackBook.UpdateScenario")
	UnregisterEventHandler(SystemData.Events.SCENARIO_END, "BlackBook.UpdateScenario")
	UnregisterEventHandler(SystemData.Events.PLAYER_DEATH_CLEARED, "BlackBook.DeathCleared")
end
 
 function BlackBook.UpdateScenario()
 
 InBG = not InBG
  
 end
 
 function BlackBook.CheckOdds(name)

 	table.foreach (BlackBook.KillersList, 
	      function (k, v)
	
			if (v["Name"] == name) then

				if(tonumber(v["Wins"]) > tonumber(v["Losses"])+4) then
					SystemData.AlertText.VecType = {16}
					SystemData.AlertText.VecText = {L"Dangerous Encounter!!"}
					AlertTextWindow.AddAlert()
					PlaySound(218)
				end
			
			end
		end
	)
 end
 
 function BlackBook.TargetUpdate(targetClassification, targetId, targetType )

  if( targetClassification ~= c_HOSTILE_TARGET or TargetInfo:UnitIsNPC(c_HOSTILE_TARGET) )
    then
        return
    end
    
    
	if(BlackBook.oldHostileName == nil) then
		BlackBook.oldHostileName = string.sub(WStringToString(TargetInfo:UnitName(c_HOSTILE_TARGET )),1,-3)
	end
	
    TargetInfo:UpdateFromClient ()

    local new = TargetInfo:UnitName(c_HOSTILE_TARGET )
    local newName = string.sub(WStringToString(TargetInfo:UnitName(c_HOSTILE_TARGET )),1,-3)


	if (new ~= old ) then
		

		if string.len(newName) > 2 then
			local recentlySeen = BlackBook.CheckRecentlySeen(newName)
		
		
			old = TargetInfo:UnitName(c_HOSTILE_TARGET )
		
			if recentlySeen ~= true then
			BlackBook.CheckOdds(newName)
				BlackBook.RecordRecentlySeen(newName)	
			
		 
			end
		 end
	end
	 
    if ( newName ~= oldHostileName ) then
	BlackBook.oldHostileName = newName
       
    end
 
 end
 
 function BlackBook.CheckRecentlySeen(name)
 
 local exists = BlackBook.RecentTargets[name]
 
 if exists == nil then
	
	return false
 else
	if exists["LastSeenTime"] < updateTime then
	
		d(updateTime)
		return false
	else
	
		return true
	end
 end
 
 end
 
 function BlackBook.RecordRecentlySeen(name)
 
 --local seenname = string.sub(WStringToString(GameData.Player.killerName),1,-3)
 BlackBook.RecentTargets[name] = {["Name"] = name, ["LastSeenTime"] = updateTime+30}
 
 end
 

 function BlackBook.DeathCleared()

 BlackBook.ResetLocals();
 
 local killername = string.sub(WStringToString(GameData.Player.killerName),1,-3)
 
 if killername == PlayerName then

BlackBook.PlayerSuicides = BlackBook.PlayerSuicides + 1
end
 
 
 end
 
 function BlackBook.PlayerDeath()

	local k = string.sub(WStringToString(GameData.Player.killerName),1,-3)
	BlackBook.UpdateKiller(k, true);
	
end

function BlackBook.PrintKillList()

	for k in pairs(BlackBook.KillersList) do
		
		local killer = BlackBook.KillersList[k]
		
		if killer.Name ~= nil then
			local outp = killer.Name.." Wins: ".. killer.Wins.." Losses: "..killer.Losses ..".. BGWins: ".. killer.BGWins.." BGLosses: "..killer.BGLoss
			TextLogAddEntry("Chat",21 , towstring(outp))
		
		end
		
	end

end

function BlackBook.ResetList()
	BlackBook.KillersList = {}
	BlackBookWindow.Reset()
end
--	The majority of the code in BlackBook.PlayerRenownUpdated() is careof the "KillingBlow" WAR Addon by HansMortis.
--	Attempts were made to contact Hans, to see if he would object to me using his algorithms to find out the player name of who we killed.
--	Since I havent received a response from Hans, and there is no user licence etc listed, I plan on using his fine work rather than re inventing the wheel.
--	Much kudos to Hans ( and then some for freeing me of my regex)
function BlackBook.PlayerRenownUpdated()

	if TextLogGetNumEntries("Combat")>2 then
		local IsExp = 0
		local line = TextLogGetNumEntries("Combat")-2
		_,_,RkillAux =TextLogGetEntry("Combat",line)

		--Language
		--We check for language through combat chat saving the language on a global var
		--cause there are some bugs with the text not showing on chatlog
		local IsLang
		_,_,RkillLang =TextLogGetEntry("Combat",line+1)
		
		if LangDef == 0 then
			local k = 1
			while k < 6 do
			
				IsLang = string.find(WStringToString(RkillLang),MsgLangKB.KEaR[k])
				if IsLang ~= nil then
					IsLang = k
					LangDef = k
					break
				end
				k = k + 1
			end
			if IsLang == nil then
				IsLang = 1
			end
		else
			IsLang = LangDef
		end
				

		if string.find(WStringToString(RkillAux),MsgLangKB.KEaR[IsLang]) ~= nil then
			line = line - 1
			IsExp = 1
		end
		
		_,_,RkillAux =TextLogGetEntry("Combat",line)
		
		local Rkill = WStringToString(RkillAux)
	
		if string.find(Rkill,MsgLangKB.KFind[IsLang]) ~= nil or string.find(Rkill,MsgLangKB.KFindAux[IsLang]) then

			if IsLang ~= 4 then
				Tkill = string.sub(Rkill,0,string.find(Rkill," "))
			else
				Tkill = string.sub(Rkill,4,string.find(Rkill," ",4))
				if Tkill == " " then
					Tkill = string.sub(Rkill,5,string.find(Rkill," ",5))
				end
			end


			if IsExp == 1 then 
				_,_,c = TextLogGetEntry("Combat",line + 1)
				local TexpAux = WStringToString(c)
				_,_,c = TextLogGetEntry("Combat",line + 2)
				local TrrAux = WStringToString(c)
				
				local Texp = string.sub(TexpAux,string.find(TexpAux,MsgLangKB.KEaR[IsLang])+string.len(MsgLangKB.KEaR[IsLang])+1,string.find(TexpAux," ",string.find(TexpAux,MsgLangKB.KEaR[IsLang])+string.len(MsgLangKB.KEaR[IsLang])+2))..""..MsgLangKB.KExp[IsLang]
				local Trr = string.sub(TrrAux,string.find(TrrAux,MsgLangKB.KEaR[IsLang])+string.len(MsgLangKB.KEaR[IsLang])+1,string.find(TrrAux," ",string.find(TrrAux,MsgLangKB.KEaR[IsLang])+string.len(MsgLangKB.KEaR[IsLang])+2))..""..MsgLangKB.KRen[IsLang]

				SystemData.AlertText.VecType = {2,16,11,12}
				SystemData.AlertText.VecText = {StringToWString(MsgLangKB.KKill[IsLang]),StringToWString(Tkill),StringToWString(Texp),StringToWString(Trr)}
			else
				_,_,c = TextLogGetEntry("Combat",line + 1)
				local TrrAux = WStringToString(c)
						
				if string.find(TrrAux,MsgLangKB.KTExp[IsLang]) ~= nil then
					local Texp = string.sub(TrrAux,string.find(TrrAux,MsgLangKB.KEaR[IsLang])+string.len(MsgLangKB.KEaR[IsLang])+1,string.find(TrrAux," ",string.find(TrrAux,MsgLangKB.KEaR[IsLang])+string.len(MsgLangKB.KEaR[IsLang])+2))..""..MsgLangKB.KExp[IsLang]
					SystemData.AlertText.VecType = {2,16,11}
					SystemData.AlertText.VecText = {StringToWString(MsgLangKB.KKill[IsLang]),StringToWString(Tkill),StringToWString(Texp)}
				else
					local Trr = string.sub(TrrAux,string.find(TrrAux,MsgLangKB.KEaR[IsLang])+string.len(MsgLangKB.KEaR[IsLang])+1,string.find(TrrAux," ",string.find(TrrAux,MsgLangKB.KEaR[IsLang])+string.len(MsgLangKB.KEaR[IsLang])+2))..""..MsgLangKB.KRen[IsLang]
					SystemData.AlertText.VecType = {2,16,12}
					SystemData.AlertText.VecText = {StringToWString(MsgLangKB.KKill[IsLang]),StringToWString(Tkill),StringToWString(Trr)}
				end
				
			end
			
			BlackBook.UpdateKiller(trim(Tkill),false)
			
			AlertTextWindow.AddAlert()
			PlaySound(218)
					
		end
		
	end

end

function BlackBook.Revenge()

SystemData.AlertText.VecType = {16}
SystemData.AlertText.VecText = {L"Revenge!!"}
PlaySound(218)

end

function BlackBook.UpdateKiller(name, win)

if name ~= PlayerName and ( string.len(name) > 2) then
local IsOnStats = 0
	table.foreach (BlackBook.KillersList, 
	      function (k, v)
	       if v["Name"] == name then
	       		IsOnStats = 1
			if win then
				v["Wins"] = v["Wins"] + 1
				
				if(v["RevengeRequired"] ~= nil) then
					v["RevengeRequired"] = 1
				end
				
			else
				v["Losses"] = v["Losses"] + 1
				
				if(v["RevengeRequired"] == 1) then
					v["RevengeRequired"] = 0
					
					BlackBook.Revenge()
				end
				
			end
	       end
	      end -- function
	      )
	if IsOnStats == 0 then
		if win then
			table.insert(BlackBook.KillersList,{["Name"] = name, ["Wins"] = 1, ["Losses"] = 0, ["BGWins"] = 0, ["BGLoss"] = 0,["RevengeRequired"]=1})
		else
			table.insert(BlackBook.KillersList,{["Name"] = name, ["Wins"] = 0, ["Losses"] = 1,["BGWins"] = 0, ["BGLoss"] = 0, ["RevengeRequired"]=0})
		end
	end



end

BlackBookWindow.UpdateList()

end
function trim(s)
	return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end