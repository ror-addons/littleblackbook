--	Utility functions careof the "KillingBlow" WAR Addon by HansMortis.
--	File and tables renamed as too not conflict with any installs of "KillingBlow"
--	Attempts were made to contact Hans, to see if he would object to me using his algorithms to find out the player name of who we killed.
--	Since I havent received a response from Hans, and there is no user licence etc listed, I plan on using his fine work rather than re inventing the wheel.
--	Much kudos to Hans ( and then some for freeing me of my regex)
--
--	I am contactable through curseforge.com if there are any issues regarding this.

MsgLangKB = {}
MsgLangKB.KFind = {}
MsgLangKB.KFindAux = {}
MsgLangKB.KFind[1] = "by "..PlayerName.."'s"  --English
MsgLangKB.KFindAux[1] = MsgLangKB.KFind[1]

MsgLangKB.KFind[2] = "de "..PlayerName  -- Spanish
MsgLangKB.KFindAux[2] = MsgLangKB.KFind[2]

MsgLangKB.KFind[3] = "de "..PlayerName  -- French
MsgLangKB.KFindAux[3] = "d'"..PlayerName  

MsgLangKB.KFind[4] = "von "..PlayerName  -- German
MsgLangKB.KFindAux[4] = MsgLangKB.KFind[4]

MsgLangKB.KFind[5] = "di "..PlayerName  -- Italian
MsgLangKB.KFindAux[5] = MsgLangKB.KFind[5]

MsgLangKB.KKill = {}
MsgLangKB.KKill[1] = "You have killed"
MsgLangKB.KKill[2] = "Has matado a"
MsgLangKB.KKill[3] = "Vous avez tu�"
MsgLangKB.KKill[4] = "Ihr erschlugt"
MsgLangKB.KKill[5] = "Hai ucciso"

MsgLangKB.KTExp = {}
MsgLangKB.KTExp[1] = "experience"
MsgLangKB.KTExp[2] = "experiencia"
MsgLangKB.KTExp[3] = "exp�rience"
MsgLangKB.KTExp[4] = "Erfahrung"
MsgLangKB.KTExp[5] = "Esperienza"


MsgLangKB.KExp = {}
MsgLangKB.KExp[1] = "XP"
MsgLangKB.KExp[2] = "Exp"
MsgLangKB.KExp[3] = "XP"
MsgLangKB.KExp[4] = "Erf"     --"Erfahrung"
MsgLangKB.KExp[5] = "XP"

MsgLangKB.KRen = {}
MsgLangKB.KRen[1] = "RP"
MsgLangKB.KRen[2] = "Rep"
MsgLangKB.KRen[3] = "RP"
MsgLangKB.KRen[4] = "RP"
MsgLangKB.KRen[5] = "PF"

MsgLangKB.KEaR = {}
MsgLangKB.KEaR[1] = "You gain"
MsgLangKB.KEaR[2] = "Has ganado"
MsgLangKB.KEaR[3] = "Vous gagnez"
MsgLangKB.KEaR[4] = "ist um"
MsgLangKB.KEaR[5] = "Guadagni"

MsgLangKB.KPts = {}
MsgLangKB.KPts[1] = "points"
MsgLangKB.KPts[2] = "puntos"
MsgLangKB.KPts[3] = "points"
MsgLangKB.KPts[4] = "Punkte"
MsgLangKB.KPts[5] = "points"

