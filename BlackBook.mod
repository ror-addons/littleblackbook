<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="BlackBook" version="1.0" date="07/08/2008" >
        
        <Author name="jiffy" email="no@email.com" />
        <Description text="PvP kill and death recorder" />
        
        <Dependencies>
	    <Dependency name="LibSlash" />
        </Dependencies>
        
        <Files>
              <File name="BlackBook.lua" />
	     <File name="BlackBookWindow.lua" />
	    <File name="BBKillingBlowText.lua" />
	    <File name="BlackBook.xml" />
        </Files>
        <OnInitialize>
	<CallFunction name="BlackBook.Initialize" />
            <CreateWindow name="BlackBookWindow" show="false" />
	  
        </OnInitialize>
	<OnUpdate>
		<CallFunction name="BlackBook.Update" />
	</OnUpdate>
	<OnShutdown>
		<CallFunction name="BlackBook.Shutdown" />
	</OnShutdown>
	<SavedVariables>
		<SavedVariable name="BlackBook.KillersList" />
		<SavedVariable name="BlackBook.PlayerSuicides" />
	</SavedVariables>
    </UiMod>
</ModuleFile>
