BlackBookWindow = {}
BlackBookWindow.entryListData = {}
BlackBookWindow.entryListOrder = {}

local windowName = "BlackBookWindow";

BlackBookWindow.SORT_ORDER_UP			= 1
BlackBookWindow.SORT_ORDER_DOWN	    	= 2

BlackBookWindow.SORT_BUTTON_NAME		= 1
BlackBookWindow.SORT_BUTTON_WIN		= 2
BlackBookWindow.SORT_BUTTON_LOSS		= 3
BlackBookWindow.SORT_MAX_NUMBER		= 3

BlackBookWindow.FILTER_ALL	    = 1


BlackBookWindow.sortButtons = {  "BlackBookWindowSortBarNameButton",		-- Order List Header 
                                    "BlackBookWindowSortBarWinButton", 
                                    "BlackBookWindowSortBarLossButton"
					}

BlackBookWindow.sortTooltip = { L"Name of enemy player", 
                                   L"# of times they have killed you!", 
                                   L"# of times you have killed them!" 
                                     }				     
				     
				 
BlackBookWindow.display = { type=BlackBookWindow.SORT_BUTTON_NAME, 
					order=BlackBookWindow.SORT_ORDER_UP, 
					filter=BlackBookWindow.FILTER_ALL }
	
	
	
local function InitializeListData()
	
	local atLeastOne = false

	BlackBookWindow.entryListData = {}
	
	if(BlackBook.KillersList ~= nil) then
	
		for key, value in ipairs( BlackBook.KillersList ) do
			 atLeastOne = true
			local killer = value
			
			BlackBookWindow.entryListData[key] = {}
			BlackBookWindow.entryListData[key].name = towstring(value.Name)
			BlackBookWindow.entryListData[key].win = value.Wins
			BlackBookWindow.entryListData[key].loss= value.Losses
	
		end
	end

	if (atLeastOne == false) then
            BlackBookWindow.entryListData[1] = {}
            BlackBookWindow.entryListData[1].name = L"NO RESULTS"
	end

end	

local function CompareEntry( index1, index2 )

	if( index2 == nil ) then
		return false
	end

	local player1 = BlackBookWindow.entryListData[index1]
	local player2 = BlackBookWindow.entryListData[index2]
    
	if (player1 == nil or player1.name == nil or player1.name == L"") then
		return false
	end
    
	if (player2 == nil or player2.name == nil or player2.name == L"") then
		return true
	end

	local type = BlackBookWindow.display.type
	local order = BlackBookWindow.display.order

	local compareResult
    
    -- Sorting by Name
	if( type == BlackBookWindow.SORT_BUTTON_NAME ) then
		if( order == BlackBookWindow.SORT_ORDER_UP ) then
			return ( WStringsCompare(player1.name, player2.name) < 0 )
		else
			return ( WStringsCompare(player1.name, player2.name) > 0 )
		end
	end
    
    -- Sort by wins
	if( type == BlackBookWindow.SORT_BUTTON_WIN )then
		if( order == BlackBookWindow.SORT_ORDER_UP ) then
			return ( tonumber(player1.win) < tonumber(player2.win) )
		else
			return ( tonumber(player1.win) > tonumber(player2.win) )
		end	
	end
    
    --Sort by loss
	if( type == BlackBookWindow.SORT_BUTTON_LOSS )then
		if( order == BlackBookWindow.SORT_ORDER_UP ) then
			return ( tonumber(player1.loss) < tonumber(player2.loss) )
		else
			return ( tonumber(player1.loss) > tonumber(player2.loss) )
		end
	end
end
	
local function SortList()	
    table.sort( BlackBookWindow.entryListOrder, CompareEntry )
end

local function FilterList()	


    
    local filter = BlackBookWindow.display.filter

    BlackBookWindow.entryListOrder = {}
    for dataIndex, data in ipairs( BlackBookWindow.entryListData ) do
        table.insert(BlackBookWindow.entryListOrder, dataIndex)
    end
end



function BlackBookWindow.UpdateList()
    -- Filter, Sort, and Update
    InitializeListData()
    BlackBookWindow.display.filter = BlackBookWindow.FILTER_ALL
    FilterList()
    SortList()
    LabelSetText("BlackBookWindowPlayerSuicidesText", towstring(BlackBook.PlayerSuicides))
    ListBoxSetDisplayOrder( "BlackBookWindowList", BlackBookWindow.entryListOrder )
end
	
function BlackBookWindow.Initialize()
	
	LabelSetText( "BlackBookWindowTitleBarText", L"My Little Black Book")
	LabelSetText("BlackBookWindowPlayerSuicides", L"Player Suicides: ")
	ButtonSetText( "BlackBookWindowSortBarNameButton",  L"Name"	)
	ButtonSetText( "BlackBookWindowSortBarWinButton",	L"Kills")
	ButtonSetText( "BlackBookWindowSortBarLossButton", L"Deaths")
	LabelSetText("BlackBookWindowPlayerSuicidesText", towstring(BlackBook.PlayerSuicides))
	BlackBookWindow.SetListRowTints()
	
	BlackBookWindow.OnSearchUpdated()
	 BlackBookWindow.UpdateSortButtons()

end

function BlackBookWindow.OnSearchUpdated()
    BlackBookWindow.ListNeedsUpdate = true
    BlackBookWindow.UpdateList()

    -- Set sort button flags
    for index = 2, BlackBookWindow.SORT_MAX_NUMBER do
        local window = BlackBookWindow.sortButtons[index]
        ButtonSetStayDownFlag( window, true )
    end
    
    BlackBookWindow.UpdateSortButtons()
end


function BlackBookWindow.Populate()

if (BlackBookWindowList.PopulatorIndices == nil) 
    then
        return
    end

    for rowIndex, dataIndex in ipairs (BlackBookWindowList.PopulatorIndices) 
    do
        local rowName = "BlackBookWindowListRow"..rowIndex

        local labelColor = DefaultColor.WHITE     
              
        LabelSetTextColor( rowName.."Name", labelColor.r, labelColor.g, labelColor.b)
        LabelSetTextColor( rowName.."Win", labelColor.r, labelColor.g, labelColor.b)
        LabelSetTextColor( rowName.."Loss", labelColor.r, labelColor.g, labelColor.b)

    end
    
    BlackBookWindow.SetListRowTints()
    BlackBookWindow.UpdateSelectedRow()
    
end


function BlackBookWindow.SetListRowTints()
    for row = 1, BlackBookWindowList.numVisibleRows do
        local row_mod = math.mod(row, 2)
        color = DataUtils.GetAlternatingRowColor( row_mod )
        
        local targetRowWindow = "BlackBookWindowListRow"..row
        WindowSetTintColor(targetRowWindow.."Background", color.r, color.g, color.b )
        WindowSetAlpha(targetRowWindow.."Background", color.a )
    end
    
end

function BlackBookWindow.UpdateSelectedRow()

    if( nil == BlackBookWindowList.PopulatorIndices )
    then
        return
    end
    
    -- Setup the Custom formating for each row
    for rowIndex, dataIndex in ipairs( BlackBookWindowList.PopulatorIndices ) 
    do    
        local selected = BlackBookWindow.SelectedPlayerDataIndex == dataIndex
        
        local rowName   = "BlackBookWindowListRow"..rowIndex

        ButtonSetPressedFlag(rowName, selected )
        ButtonSetStayDownFlag(rowName, selected )
    end
    
end

-- Displays the clicked sort button as pressed down and positions an arrow above it
function BlackBookWindow.UpdateSortButtons()
    
    local type = BlackBookWindow.display.type
    local order = BlackBookWindow.display.order

    for index = 2, BlackBookWindow.SORT_MAX_NUMBER do
        local window = BlackBookWindow.sortButtons[index]
        ButtonSetPressedFlag( window, index == BlackBookWindow.display.type )
    end
    
	WindowSetShowing( "BlackBookWindowSortBarUpArrow", order == BlackBookWindow.SORT_ORDER_UP )
          WindowSetShowing( "BlackBookWindowSortBarDownArrow", order == BlackBookWindow.SORT_ORDER_DOWN )
    
      local window = BlackBookWindow.sortButtons[type]
    
     if( order == BlackBookWindow.SORT_ORDER_UP ) then		
            WindowClearAnchors( "BlackBookWindowSortBarUpArrow" )
            WindowAddAnchor("BlackBookWindowSortBarUpArrow", "left", window, "left", 0, 0 )
        else
            WindowClearAnchors( "BlackBookWindowSortBarDownArrow" )
            WindowAddAnchor("BlackBookWindowSortBarDownArrow", "right",window, "right", 0, 0 )
        end
end

-- Callback for hovering over a sort button
function BlackBookWindow.OnMouseOverSortButton()
    local windowName	= SystemData.ActiveWindow.name
    local windowIndex	= WindowGetId (windowName)

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, BlackBookWindow.sortTooltip[windowIndex] )
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_HEADING)	
    Tooltips.Finalize ()
    
    local anchor = { Point="top", RelativeTo=windowName, RelativePoint="center", XOffset=0, YOffset=-32 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end

-- Callback for clicking on a sort button
function BlackBookWindow.OnSortList()

 local type = WindowGetId( SystemData.ActiveWindow.name )
    -- If we are already using this sort type, toggle the order.
    if( type == BlackBookWindow.display.type ) then
        if( BlackBookWindow.display.order == BlackBookWindow.SORT_ORDER_UP ) then
            BlackBookWindow.display.order = BlackBookWindow.SORT_ORDER_DOWN
        else
            BlackBookWindow.display.order = BlackBookWindow.SORT_ORDER_UP
        end
        
    -- Otherwise change the type and use the up order.	
    else
        BlackBookWindow.display.type = type
        BlackBookWindow.display.order = BlackBookWindow.SORT_ORDER_UP
    end

    BlackBookWindow.playerListNeedsUpdate = true
    BlackBookWindow.UpdateList()
    BlackBookWindow.UpdateSortButtons()

end

function BlackBookWindow.OnHidden()
	
end


function BlackBookWindow.Reset()

BlackBookWindow.entryListData = {}
BlackBookWindow.entryListOrder = {}

end


function BlackBookWindow.OnLButtonUpMemberRow()
	
end

function BlackBookWindow.OnShown()
	

end

function BlackBookWindow.OnUpdate()

end

function BlackBookWindow.OnLButtonUpClose()
	
	if( not WindowGetShowing( windowName ) )
    then
        return
    end

    WindowSetShowing( windowName, false )
    
    Sound.Play( Sound.WINDOW_CLOSE )
    
    WindowUtils.RemoveFromOpenList( windowName )
	
end

function BlackBookWindow.Show()

 if( WindowGetShowing( windowName) )
    then
        return
    end
    
    BlackBookWindow.UpdateList()
    
    WindowSetShowing( windowName, true)
    
    WindowUtils.AddToOpenList( windowName, BlackBookWindow.OnLButtonUpClose, WindowUtils.Cascade.MODE_NONE )

    Sound.Play( Sound.WINDOW_OPEN )

end